﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using NUnit.Framework;
using Kendo.Mvc.Grid.CRUD.Models;
using System.IO;


namespace TestTask
{    
    [TestFixture]    
    public class UnitTesting 
    {     
        ProductionController productionController = new ProductionController();
        ApplyingController applyingController = new ApplyingController();
        
        public UnitTesting()
        {                        
            AppDomain.CurrentDomain.SetData("DataDirectory", 
                 Path.Combine(
                   Directory.GetParent(Directory.GetCurrentDirectory().ToString()).ToString(),
                   "App_Data")                
                );           
        }
        private void ProductionCountAssert(Northwind northwind, long start, long finish, int num)
        {
            productionController.Create(new ProductionViewModel { ID = 0, Comment = "comment", Start = start, Finish = finish });
            Assert.AreEqual(northwind.Productions.ToList<Production>().Count(), num);          
        }

        private void ApplyingCountAssert(Northwind northwind, long start, long finish, int num)
        {
            applyingController.Create(new ApplyingViewModel { ID = 0, Comment = "comment2", Start = start, Finish = finish });
            Assert.AreEqual(northwind.Applyings.ToList<Applying>().Count(), num);
        }

        [Test]
        public void InsertTest()
        {                      
           using (var northwind = new Northwind())
           {
               northwind.ClearData();
               ProductionCountAssert(northwind, 999999999, 999999999, 0);
               ProductionCountAssert(northwind, 999999999, 1000000000, 0);
               ProductionCountAssert(northwind, 999999999, 1000000000, 0);
               ProductionCountAssert(northwind, 9999999999, 10000000000, 0);                
               ProductionCountAssert(northwind, 1000000001, 1000000000, 0);
               ProductionCountAssert(northwind, 1000000000, 1000000000, 1);
               ProductionCountAssert(northwind, 1000000002, 1000000003, 2);
               ProductionCountAssert(northwind, 2000000000, 2000000010, 3);
               ProductionCountAssert(northwind, 3000000000, 3000000010, 4);
               ProductionCountAssert(northwind, 1000000004, 2000000000, 4);
               ProductionCountAssert(northwind, 1000000004, 2000000001, 4);
               ProductionCountAssert(northwind, 1000000004, 2000000011, 4);
               ProductionCountAssert(northwind, 1000000004, 3000000000, 4);
               ProductionCountAssert(northwind, 1000000004, 3000000011, 4);
               ProductionCountAssert(northwind, 1000000010, 1000000111, 5);
               ProductionCountAssert(northwind, 1000000008, 1000000009, 6);
               ProductionCountAssert(northwind, 1000000007, 1000000007, 7);
               ProductionCountAssert(northwind, 1000000006, 1000000006, 8);               

               ApplyingCountAssert(northwind, 999999999, 999999999, 0);
               ApplyingCountAssert(northwind, 999999999, 1000000000, 0);
               ApplyingCountAssert(northwind, 999999999, 1000000000, 0);
               ApplyingCountAssert(northwind, 9999999999, 10000000000, 0);
               ApplyingCountAssert(northwind, 1000000001, 1000000000, 0);
               ApplyingCountAssert(northwind, 1000000000, 1000000000, 1);
               ApplyingCountAssert(northwind, 1000000000, 1000000000, 1);
               ApplyingCountAssert(northwind, 1000000005, 1000000006, 1);
               ApplyingCountAssert(northwind, 1000000006, 1000000019, 2);
               ApplyingCountAssert(northwind, 1000000006, 1000000006, 2);
               ApplyingCountAssert(northwind, 1000000007, 1000000010, 2);
               ApplyingCountAssert(northwind, 1000000007, 1000000009, 2);
               ApplyingCountAssert(northwind, 3000000003, 3000000008, 3);
               ApplyingCountAssert(northwind, 3000000000, 3000000000, 4);
               ApplyingCountAssert(northwind, 3000000001, 3000000002, 5);
               ApplyingCountAssert(northwind, 1000000025, 1000000035, 6);
           }            
        }

        [Test]
        public void UpdateTest()
        {
            using (var northwind = new Northwind())
            {
                northwind.ClearData();
                ProductionCountAssert(northwind, 3000000000, 3000000000, 1);
                ApplyingCountAssert(northwind, 3000000000, 3000000000, 1);
                
                productionController.Update(new ProductionViewModel { ID = northwind.Productions.ToList<Production>()[0].ID, Comment = "mod", Start = 4000000000, Finish = 5000000000 });
                
                applyingController.Update(new ApplyingViewModel { ID = northwind.Applyings.ToList<Applying>()[0].ID, Comment = "mod2", Start = 4000000000, Finish = 5000000000 });
            }
            using (var northwind = new Northwind())
            {
                var rec = northwind.Productions.ToList<Production>()[0];
                Assert.AreEqual(rec.Start, 4000000000);
                Assert.AreEqual(rec.Finish, 5000000000);
                Assert.AreEqual(rec.Comment, "mod");

                var rec2 = northwind.Applyings.ToList<Applying>()[0];
                Assert.AreEqual(rec2.Start, 4000000000);
                Assert.AreEqual(rec2.Finish, 5000000000);
                Assert.AreEqual(rec2.Comment, "mod2");                
            }
        }


         [Test]
        public void DestroyTest()
        {
            using (var northwind = new Northwind())
            {
                northwind.ClearData();
                ProductionCountAssert(northwind, 3000000000, 3000000000, 1);
                ApplyingCountAssert(northwind, 3000000000, 3000000000, 1);

                ProductionCountAssert(northwind, 4000000000, 4000000000, 2);
                ApplyingCountAssert(northwind, 4000000000, 4000000000, 2);

                productionController.Destroy(new ProductionViewModel { ID = northwind.Productions.ToList<Production>()[0].ID });
                applyingController.Destroy(new ApplyingViewModel { ID = northwind.Applyings.ToList<Applying>()[0].ID });
            }
            using (var northwind = new Northwind())
            {                
                Assert.AreEqual(northwind.Productions.ToList<Production>().Count(), 1);
                Assert.AreEqual(northwind.Applyings.ToList<Applying>().Count(), 1);                
            }
        }
   

    }
}
