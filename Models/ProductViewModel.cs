﻿namespace Kendo.Mvc.Grid.CRUD.Models
{
    public partial class ProductionViewModel
    {
        public int ID { get; set; }
        public long Start { get; set; }
        public long Finish { get; set; }
        public string Comment { get; set; }
    }

    public partial class ApplyingViewModel
    {
        public int ID { get; set; }
        public long Start { get; set; }
        public long Finish { get; set; }
        public string Comment { get; set; }
    }

    public partial class BallanceViewModel
    {
        public int ID { get; set; }
        public long Start { get; set; }
        public long Finish { get; set; }        
    }
}