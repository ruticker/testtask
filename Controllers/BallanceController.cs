﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Grid.CRUD.Models;
using Kendo.DynamicLinq;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;

namespace TestTask
{
    public class BallanceController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
       
        [HttpPost]
        public ActionResult Read(int take, int skip, IEnumerable<Sort> sort, Kendo.DynamicLinq.Filter filter)
        {
            using (var northwind = new Northwind())
            {
                var r = northwind.Getbalances().ToList<Ballance>().AsQueryable();
                var result = r
                    .OrderBy(p => p.ID)                     
                    .Select(p => new BallanceViewModel
                    {   ID = p.ID,                     
                        Start = p.Start,
                        Finish = p.Finish                        
                    }).ToDataSourceResult(take, skip, sort, filter);
                return Json(result);
            }
        }    
    }
}