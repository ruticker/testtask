﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Grid.CRUD.Models;
using Kendo.DynamicLinq;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;

namespace TestTask
{
    public class ApplyingController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ApplyingViewModel applyingViewModel)
        {
            var result = new List<Applying>();
            using (var northwind = new Northwind())
            {
                if (northwind.CanInsertApplying(applyingViewModel.Start, applyingViewModel.Finish).ElementAt(0).Value == 0)
                {
                    ModelState.AddModelError("error", "error");
                    return View();
                }

                var applying = new Applying
                {
                    Comment = applyingViewModel.Comment,
                    Start = applyingViewModel.Start,
                    Finish = applyingViewModel.Finish
                };
                result.Add(applying);
                northwind.Applyings.Add(applying);
                northwind.SaveChanges();
                return Json(result.Select(p => new ApplyingViewModel
                {
                    ID = p.ID,
                    Comment = p.Comment,
                    Start = p.Start,
                    Finish = p.Finish
                }).ToList());
            }
        }

        [HttpPost]
        public ActionResult Read(int take, int skip, IEnumerable<Sort> sort, Kendo.DynamicLinq.Filter filter)
        {
            using (var northwind = new Northwind())
            {
                var result = northwind.Applyings
                    .OrderBy(p => p.ID)
                    .Select(p => new ApplyingViewModel
                    {
                        ID = p.ID,
                        Comment = p.Comment,
                        Start = p.Start,
                        Finish = p.Finish
                    })
                    .ToDataSourceResult(take, skip, sort, filter);

                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult Update(ApplyingViewModel applyingViewModel)
        {
            ModelState.AddModelError("ProductName", "Error");
            using (var northwind = new Northwind())
            {
                /*
                if (northwind.CanInsertApplying(applyingViewModel.Start, applyingViewModel.Finish).ElementAt(0).Value == 0)
                {
                    ModelState.AddModelError("error", "error");
                    return View();
                }
                 */
                var applying = new Applying
                {
                    ID = applyingViewModel.ID,
                    Comment = applyingViewModel.Comment,
                    Start = applyingViewModel.Start,
                    Finish = applyingViewModel.Finish
                };

                northwind.Applyings.Attach(applying);
                northwind.Entry(applying).State = System.Data.EntityState.Modified;
                northwind.SaveChanges();
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult Destroy(ApplyingViewModel applyingViewModel)
        {
            using (var northwind = new Northwind())
            {
                var applying = new Applying
                {
                    ID = (int)applyingViewModel.ID,
                };
                northwind.Applyings.Attach(applying);
                northwind.Applyings.Remove(applying);
                northwind.SaveChanges();
                return Json(null);
            }
        }

    }
}