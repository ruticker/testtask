﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Grid.CRUD.Models;
using Kendo.DynamicLinq;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;

namespace TestTask
{
    public class ProductionController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ProductionViewModel productionViewModel)
        {
            var result = new List<Production>();
            using (var northwind = new Northwind())
            {

                if (northwind.CanInsertProduction(productionViewModel.Start, productionViewModel.Finish).ElementAt(0).Value == 0)
                {
                    ModelState.AddModelError("error", "error");
                    return View();
                }

                var production = new Production
                {
                    Comment = productionViewModel.Comment,
                    Start = productionViewModel.Start,
                    Finish = productionViewModel.Finish
                };
                result.Add(production);
                northwind.Productions.Add(production);
                northwind.SaveChanges();

                return Json(result.Select(p => new ProductionViewModel
                {
                    ID = p.ID,
                    Comment = p.Comment,
                    Start = p.Start,
                    Finish = p.Finish
                })
                .ToList());
            }
        }

        [HttpPost]
        public ActionResult Read(int take, int skip, IEnumerable<Sort> sort, Kendo.DynamicLinq.Filter filter)
        {
            using (var northwind = new Northwind())
            {
                var result = northwind.Productions
                    .OrderBy(p => p.ID)
                    .Select(p => new ProductionViewModel
                    {
                        ID = p.ID,
                        Comment = p.Comment,
                        Start = p.Start,
                        Finish = p.Finish
                    })
                    .ToDataSourceResult(take, skip, sort, filter);

                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult Update(ProductionViewModel productionViewModel)
        {
            using (var northwind = new Northwind())
            {
                /*
                if (northwind.CanInsertProduction(productionViewModel.Start, productionViewModel.Finish).ElementAt(0).Value == 0)
                {
                    ModelState.AddModelError("error", "error");
                    return View();
                }
                */
                var production = new Production
                {
                    ID = productionViewModel.ID,
                    Comment = productionViewModel.Comment,
                    Start = productionViewModel.Start,
                    Finish = productionViewModel.Finish
                };
                northwind.Productions.Attach(production);
                northwind.Entry(production).State = System.Data.EntityState.Modified;

                northwind.SaveChanges();
                return Json(null);
            }
        }

        [HttpPost]
        public ActionResult Destroy(ProductionViewModel productionViewModel)
        {
            using (var northwind = new Northwind())
            {
                var production = new Production
                {
                    ID = (int)productionViewModel.ID,
                };
                northwind.Productions.Attach(production);
                northwind.Productions.Remove(production);

                northwind.SaveChanges();
                return Json(null);
            }
        }

    }
}