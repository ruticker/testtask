
CREATE FUNCTION [dbo].[IntersectApplying]
(
	@start bigint, @finish bigint
)
RETURNS int

AS
BEGIN
	if not (@start  between 1000000000 and 9999999999)		RETURN 0;
	
	if not (@finish between 1000000000 and 9999999999)		RETURN 0;
	if (@start > @finish)		RETURN 0;
	
	declare @cnt int;	
	set @cnt = 0;
	
	select @cnt = COUNT(*) from Applying where Finish >= @start and Start <= @finish;
	
	if (@cnt != 0)
		return 0;

	return 1;
END

GO

CREATE FUNCTION [dbo].[IntersectApplying2]
(
	@start bigint, @finish bigint
)
RETURNS int

AS
BEGIN
	if not (@start  between 1000000000 and 9999999999)		RETURN 0;
	
	if not (@finish between 1000000000 and 9999999999)		RETURN 0;
	if (@start > @finish)		RETURN 0;
	
	declare @cnt int;		
	set @cnt = 0;
	
	declare @s bigint;		
	set @s = 0;
	
	declare @f bigint;		
	set @f = 0;
	
	
	select top 1 @s = start, @f = finish from Production where start <= @start and Finish >= @start order by Start desc
	
	if (@s = 0) return 0;
	
	if (@f >= @finish) return 1;

	return dbo.[IntersectApplying2](@f + 1, @finish);
END

CREATE FUNCTION [dbo].[IntersectProduction]
(
	@start bigint, @finish bigint
)
RETURNS int

AS
BEGIN
	if not (@start  between 1000000000 and 9999999999)		RETURN 0;
	
	if not (@finish between 1000000000 and 9999999999)		RETURN 0;
	if (@start > @finish)		RETURN 0;
	
	declare @cnt int;	
	set @cnt = 0;
	
	select @cnt = COUNT(*) from Production where Finish >= @start and Start <= @finish;
	
	if (@cnt != 0)
		return 0;

	return 1;
END

CREATE TABLE [dbo].[Applying](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Start] [bigint] NOT NULL,
	[Finish] [bigint] NOT NULL,
	[Comment] [varchar](50) NULL,
 CONSTRAINT [PK_Applying] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Ballance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Start] [bigint] NOT NULL,
	[Finish] [bigint] NOT NULL,
 CONSTRAINT [PK_Ballance] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[Production](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Start] [bigint] NOT NULL,
	[Finish] [bigint] NOT NULL,
	[Comment] [varchar](50) NULL,
 CONSTRAINT [PK_Production] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE Procedure [dbo].[CanInsertApplying](	@start bigint, @finish bigint)
AS
BEGIN
   select dbo.IntersectApplying(@start,@finish) * dbo.IntersectApplying2(@start,@finish)
END


CREATE Procedure [dbo].[CanInsertProduction](	@start bigint, @finish bigint)
AS
BEGIN
   select dbo.IntersectProduction(@start,@finish)
END

CREATE PROCEDURE [dbo].[ClearData] 
AS 
  delete from Production
  delete from Applying
GO

CREATE PROCEDURE [dbo].[Getbalances] 
AS 
  BEGIN 
      DECLARE @S INT 
      DECLARE @F INT 
      DECLARE @S1 BIGINT 
      DECLARE @F1 BIGINT 
      DECLARE @S2 BIGINT 
      DECLARE @F2 BIGINT 
      /*��������� ������*/ 
      DECLARE @CURSOR CURSOR 

      /*��������� ������*/ 
      CREATE TABLE #temp 
        ( 
		   [ID] [int] IDENTITY(1,1) NOT NULL,
           start  BIGINT not null, 
           finish BIGINT not null
        ) 

      SET @CURSOR = CURSOR scroll 
      FOR SELECT start, 
                 finish 
          FROM   production 
          ORDER  BY start 

      /*��������� ������*/ 
      OPEN @CURSOR 

      /*�������� ������ ������*/ 
      FETCH next FROM @CURSOR INTO @S1, @F1 

      /*��������� � ����� ������� �����*/ 
      IF ( @@FETCH_STATUS = 0 ) 
	  begin
        IF ( @s1 > 1000000000 ) 
          
              INSERT INTO #temp 
                          (start, finish) VALUES     (1000000000,  @s1 - 1) 
       end
        ELSE 
          INSERT INTO #temp 
                      (start, 
                       finish) 
          VALUES     (1000000000, 
                      9999999999) 

      WHILE @@FETCH_STATUS = 0 
        BEGIN             
            FETCH next FROM @CURSOR INTO @S2, @F2 
            IF ( @s2 - @f1 > 1 ) INSERT INTO #temp  (start, finish)  VALUES     (@f1 + 1,          @s2 - 1)               
            SET @f1 = @f2 
            SET @s1 = @s2 
        END 

      CLOSE @CURSOR 

      IF ( @f2 < 9999999999 ) INSERT INTO #temp  (start, finish)  VALUES (@f2 + 1,  9999999999) 
        

      SELECT * 
      FROM   #temp 
      ORDER  BY start 
  END 

